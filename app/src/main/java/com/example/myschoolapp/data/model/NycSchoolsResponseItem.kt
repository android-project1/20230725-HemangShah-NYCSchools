package com.example.myschoolapp.data.model

data class NycSchoolsResponseItem(

    val school_10th_seats: String,
    val school_accessibility_description: String,
    val school_email: String,
    val school_name: String,
    val school_sports: String,
    val dbn: String,
    val boys: String,
    val bus: String,
    val campus_name: String,
    val city: String,
    val girls: String,
    val latitude: String,
    val location: String,
    val longitude: String,
    val website: String,
    val start_time: String,
    val end_time: String,

)