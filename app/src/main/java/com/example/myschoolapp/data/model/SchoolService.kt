package com.example.myschoolapp.data.model

import retrofit2.http.GET

interface SchoolService {

    @GET("s3k6-pzi2.json")
     suspend fun getSchoolInfo(): ArrayList<NycSchoolsResponseItem>

     @GET("f9bf-2cp4.json")
     suspend fun getSATInfoFromSchool(): ArrayList<NycSchoolSatScoreResponseItem>
}