package com.example.myschoolapp.di

import com.example.myschoolapp.data.model.SchoolService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    fun provideAnalyticsService(): SchoolService {
        return Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us/resource/")
            .client(OkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SchoolService::class.java)
    }
//https://data.cityofnewyork.us/resource/f9bf-2cp4.json - SAT
//https://data.cityofnewyork.us/resource/s3k6-pzi2.json - school
}