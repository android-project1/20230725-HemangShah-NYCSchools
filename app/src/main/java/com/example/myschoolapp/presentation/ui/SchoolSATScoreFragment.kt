package com.example.myschoolapp.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.myschoolapp.R
import com.example.myschoolapp.databinding.FragmentSchoolSatScoreBinding
import com.example.myschoolapp.presentation.viewmodel.SchoolSATScoreViewModel
import dagger.hilt.android.AndroidEntryPoint

private const val SCHOOL_DBN = "SCHOOL_DBN"

@AndroidEntryPoint
class SchoolSATScoreFragment : Fragment() {

    private var schoolDBN: String? = null
    private var _binding: FragmentSchoolSatScoreBinding? = null
    private val viewModel by viewModels<SchoolSATScoreViewModel>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            schoolDBN = it.getString(SCHOOL_DBN)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSchoolSatScoreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.schoolSATInfoLiveData.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = View.GONE
            binding.schoolDetailName.text = it.school_name
            binding.schoolTestTakers.text = buildString {
                append(getString(R.string.number_of_students))
                append(it.num_of_sat_test_takers)
            }
            binding.schoolMathScore.text = buildString {
                append(getString(R.string.average_maths_score))
                append(it.sat_math_avg_score)
            }
            binding.schoolReadingScore.text =
                buildString {
                    append(getString(R.string.average_reading_score))
                    append(it.sat_critical_reading_avg_score)
                }
            binding.schoolWritingScore.text = buildString {
                append(getString(R.string.average_writing_score))
                append(it.sat_writing_avg_score)
            }
        }
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(requireContext(), "Could not find details", Toast.LENGTH_SHORT).show()
            }
        }
        schoolDBN?.let { viewModel.getSATScore(it) }
    }

}