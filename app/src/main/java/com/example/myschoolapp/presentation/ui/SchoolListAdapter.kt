package com.example.myschoolapp.presentation.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myschoolapp.data.model.NycSchoolsResponseItem
import com.example.myschoolapp.databinding.ListItemBinding

class SchoolListAdapter(private val onClick: (NycSchoolsResponseItem) -> Unit) :
    ListAdapter<NycSchoolsResponseItem, SchoolListAdapter.SchoolListViewHolder>(SchoolDiffCallback) {

    class SchoolListViewHolder(
        itemView: ListItemBinding,
        val onClick: (NycSchoolsResponseItem) -> Unit
    ) :
        RecyclerView.ViewHolder(itemView.root) {
        private val schoolName: TextView = itemView.schoolName
        private val schoolBus: TextView = itemView.schoolBus
        private val schoolCity: TextView = itemView.schoolCity
        private val schoolCampus: TextView = itemView.schoolCampus
        private val schoolEmail: TextView = itemView.schoolEmail
        private val schoolWebsite: TextView = itemView.schoolWebsite
        private val schoolLocation: TextView = itemView.schoolLocation
        private val schoolTime: TextView = itemView.schoolTime


        private var currentSchool: NycSchoolsResponseItem? = null

        init {
            itemView.root.setOnClickListener {
                currentSchool?.let {
                    onClick(it)
                }
            }
        }

        /* Bind School Name. */
        fun bind(school: NycSchoolsResponseItem) {
            currentSchool = school
            schoolName.text = school.school_name
            schoolBus.text = school.bus
            schoolBus.isSelected = true
            schoolCity.text = school.city
            if (school.campus_name.isNullOrBlank()) {
                schoolCampus.visibility = View.GONE
            } else {
                schoolCampus.visibility = View.VISIBLE
                schoolCampus.text = school.campus_name
            }
            schoolEmail.text = school.school_email
            if (school.start_time.isNullOrBlank() || school.end_time.isNullOrBlank()) {
                schoolTime.visibility = View.GONE
            } else {
                schoolTime.visibility = View.VISIBLE
                schoolTime.text = buildString {
                    append(school.start_time)
                    append(" - ")
                    append(school.end_time)
                }
            }
            schoolWebsite.text = school.website
            schoolLocation.text = school.location.slice(0 until school.location.indexOf('('))
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolListViewHolder {

        val view = ListItemBinding.inflate(LayoutInflater.from(parent.context))
        return SchoolListViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: SchoolListViewHolder, position: Int) {
        val schoolList = getItem(position)
        holder.bind(schoolList)
    }
}

object SchoolDiffCallback : DiffUtil.ItemCallback<NycSchoolsResponseItem>() {
    override fun areItemsTheSame(
        oldItem: NycSchoolsResponseItem,
        newItem: NycSchoolsResponseItem
    ): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: NycSchoolsResponseItem,
        newItem: NycSchoolsResponseItem
    ): Boolean {
        return oldItem.dbn == newItem.dbn
    }

}