package com.example.myschoolapp.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myschoolapp.data.model.NycSchoolSatScoreResponseItem
import com.example.myschoolapp.domain.usecase.GetNycSchoolUseCase
import com.example.myschoolapp.domain.usecase.GetSchoolSatInfoUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolSATScoreViewModel @Inject constructor(
    private val schoolSATUseCase: GetSchoolSatInfoUseCase
) :
    ViewModel() {
    val schoolSATInfoLiveData = MutableLiveData<NycSchoolSatScoreResponseItem>()
    val errorLiveData = MutableLiveData<Boolean>(false)
    fun getSATScore(dbn: String = "01M292") {
        viewModelScope.launch {
            //here i would prefer to use sealed class for different response like Success, Progress, Error etc to manage all the scenario
            try {
                val selectedItemDetails = schoolSATUseCase.invoke().filter { it.dbn == dbn }
                schoolSATInfoLiveData.postValue(selectedItemDetails[0])
                errorLiveData.postValue(false)
            } catch (e: Exception) {
                println(e.localizedMessage)
                errorLiveData.postValue(true)
            }
        }
    }
}