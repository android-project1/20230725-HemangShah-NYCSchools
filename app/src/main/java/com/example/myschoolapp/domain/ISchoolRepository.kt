package com.example.myschoolapp.domain

import com.example.myschoolapp.data.model.NycSchoolSatScoreResponseItem
import com.example.myschoolapp.data.model.NycSchoolsResponseItem

interface ISchoolRepository {

    suspend fun getSchools(): ArrayList<NycSchoolsResponseItem>

    suspend fun getSATInfoFromSchool(): ArrayList<NycSchoolSatScoreResponseItem>
}