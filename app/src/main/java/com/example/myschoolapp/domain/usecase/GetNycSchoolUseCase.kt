package com.example.myschoolapp.domain.usecase

import com.example.myschoolapp.data.model.NycSchoolsResponseItem
import com.example.myschoolapp.data.repository.SchoolRepository
import javax.inject.Inject

class GetNycSchoolUseCase @Inject constructor(
    private val schoolRepository: SchoolRepository
) {

    suspend operator fun invoke(): ArrayList<NycSchoolsResponseItem> {
        return schoolRepository.getSchools()
    }

}