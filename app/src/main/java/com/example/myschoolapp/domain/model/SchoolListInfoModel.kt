package com.example.myschoolapp.domain.model

data class SchoolListInfoModel(
    val Description: String,
    val cityName: String,
    val ID: Int
) {
    // This is the Domain Model
    //based on UI requirement we can use mapper and convert data model to domain model
}

