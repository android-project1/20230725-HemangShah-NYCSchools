# 20230725-HemangShah-NYCSchools



## NYC School App
---

NYC School application which shows list of schools in NYC area. The app is build in Native Android using MVVM with Clean architecture.
</br>

## Tech stack & Open-source libraries
---
- Minimum SDK level 24
- Kotlin
- Coroutines
- Hilt for dependency injection.
- JetPack
    - ViewModel
    - LiveData
- Architecture
    - MVVM with clean Architecture (Model - View - ViewModel)
- Retrofit2
- Mockito